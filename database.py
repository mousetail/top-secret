import sqlite3
import os

def execute_query(query):
    loc='file://localhost/'+(os.path.join(os.getcwd(), 'db.sqlite3')).replace('\\','/')+'?mode=ro'
    print(loc)
    database = sqlite3.connect(loc, uri=True)
    cursor = database.cursor()
    
    

    
    print(query)
    cursor.execute(
        query.split(';')[-1]+';'
    )
    
    data = cursor.fetchall()
    cursor.close()
    database.close()
    return data

def subject_query(name=None, location=None, age=None, agent=None):
    query = ('SELECT "", subjects.name, subjects.age, subjects.location,'
            'agents.codename'
             ' FROM subjects INNER JOIN agents ON agents.id=subjects.handler_id WHERE 1=1')
    if name is not None and name!='':
        query+=' AND subjects.name LIKE "%'+name+'%"'
    if location is not None and location!='':
        query+=' AND subjects.location LIKE "%'+location+'%"'
    if age is not None and age !='':
        query+=' AND subjects.age='+age+''
    if agent:
        query+=' AND agents.codename LIKE "%'+agent+'%"'
    return execute_query(query)

def organization_query(name=None, location=None):
    query = ('SELECT pages.url, organizations.name, organizations.location FROM '
             'organizations INNER JOIN pages ON organizations.page_id = pages.id'
             ' WHERE 1=1')
    if name is not None and name:
        query+=' AND organizations.name LIKE "%'+name+'%"'
    if location is not None:
        query+=' AND organizations.location LIKE "%'+location+'%"'


    return execute_query(query)

def agent_query(name=None, location=None, age=None, codename=None):
    query = ('SELECT url, name, age, codename, location FROM'
             ' agents INNER JOIN pages ON agents.page_id=pages.id WHERE 1=1')
    if name:
        query+=' AND name LIKE "%'+name+'%"'
    if location:
        query+=' AND location LIKE "%'+location+'%"'
    if age is not None and age !='':
        query+=' AND subjects.age='+age+''
    if codename:
        query+=' AND location LIKE "%'+codename+'%"'

    return execute_query(query)

def page_query(url=None):
    query = ("SELECT url, url FROM PAGES WHERE 1=1")
    if url:
        query+=' AND url LIKE "%'+url+'%"'

    return execute_query(query)

if __name__=="__main__":
    print(execute_query())
    
