import pygame, random, json
from html import escape

names = ['Anthony ', 'Josiah ', 'Emmett ', 'Jameson ', 'Tristan ', 'Nicholas ', 'Carson ', 'Miles ', 'Bennett ', 'Diego ',
         'Jackson ', 'Jonathan ', 'Jaxon ', 'Jeremiah ', 'Rowan ', 'Austin ', 'Ayden ', 'Carlos ', 'Thomas ', 'Nathaniel ',
         'Easton ', 'Jace ', 'John ', 'Hudson ', 'Chase ', 'Ryker ', 'Gabriel ', 'Jason ', 'Robert ', 'Luca ', 'Declan ',
         'Landon ', 'Vincent ', 'Greyson ', 'Bryson ', 'Damian ', 'Lucas ', 'Leo ', 'Isaiah ', 'Xavier ', 'Sebastian ',
         'Ryan ', 'Cole ', 'Roman ', 'William ', 'Andrew ', 'Julian ', 'George ', 'Jose ', 'Colton ', 'Silas ', 'Luis ',
         'Mateo ', 'Matthew ', 'James ', 'Adrian ', 'Christopher ', 'David ', 'Isaac ', 'Noah ', 'Zachary ', 'Juan ', 'Luke ',
         'Kaiden ', 'Christian ', 'Dominic ', 'Ivan ', 'Tyler ', 'Grayson ', 'Asher ', 'Henry ', 'Ezra ', 'Eli ', 'Kai ', 'Harrison ',
         'Caleb ', 'Oliver ', 'Brandon ', 'Cooper ', 'Nathan ', 'Michael ', 'Jacob ', 'Ian ', 'Liam ', 'Ethan ', 'Weston ', 'Lincoln ',
         'Adam ', 'Gavin ', 'Hunter ', 'Benjamin ', 'Nolan ', 'Samuel ', 'Jack ', 'Joseph ', 'Santiago ', 'Micah ', 'Owen ', 'Evan ',
         'Gael ', 'Jordan ', 'Axel ', 'Ezekiel ', 'Brayden ', 'Jaxson ', 'Kevin ', 'Everett ', 'Wyatt ', 'Daniel ', 'Maxwell ', 'Leonardo ',
         'Connor ', 'Max ', 'Carter ', 'Ashton ', 'Ryder ', 'Elijah ', 'Mason ', 'Jayden ', 'Kingston ', 'Maverick ', 'Alexander ', 'Sawyer ',
         'Elias ', 'Aiden ', 'Levi ', 'Theodore ', 'Logan ', 'Angel ', 'Dylan ', 'Kayden ', 'Aaron ', 'Wesley ', 'Bentley ', 'Braxton ',
         'Cameron ', 'Joshua ', 'Charles ', 'Parker ']
         
locations = [

]

def generate_locations():
    image = pygame.image.load('resources/gps.png')
    
    for i in range(500):
        valid = False
        while not valid:     
            px = random.randrange(0, image.get_width())
            py = random.randrange(0, image.get_height())
            pixel = image.get_at((px, py))
            if (pixel[0]<128 and pixel[3]>128):
                valid=True
        
        locations.append((py*45/image.get_width(), px*45/image.get_width()))
        
    with open('resources/trace2.xml', 'w') as f:
        f.write("""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
                <gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" creator="Oregon 400t" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd">
        <trk>
        <name>Secret</name>
        """)
        
        for i in locations:
            f.write("""
            <trkseg><trkpt lat='{}' lon='{}'/></trkseg>""".format(
                *i)
            )
            
        f.write("""
        </trk>
        </gpx>""")
    
    
        

def generate_names():
    with open('resources/firstnames.json') as f:
        names = json.load(f)
        
    with open('resources/adminpage.html') as f:
        adminpage = f.read()
    
    nms = ""
    
    locations2 = locations[:]
    for i in range(100):
        locations2.append(None)
    locations2 = [(random.choice(names), i) for i in locations2]
       
    locations2.sort(key=lambda i: i[0])
    
    for name, point in locations2:
        badges = []
        if point is not None:
            badges.append("<div class='user-badge green'>{}</div>".format(escape("lat: {:.2f}, long: {:.2f}".format(*point))))
        if random.randint(0,3)==0:
            badges.append("<div class='user-badge blue'>On Leave</div>")
        if random.randint(0,20)==0:
            badges.append("<div class='user-badge red'>Admin</div>")
        if random.randint(0,60)==0:
            badges.append("<div class='user-badge red'>Suspected Compromised</div>")
        
        random.shuffle(badges)
    
    
        n = """<div class='user'>
		<img class='user-picture' src='/static/p256.png'>
		<div class='user-name'>{}</div>
        {}
        </div>
        """.format(
            escape(name),
            ''.join(badges)
        )
        nms+=n
    adminpage = adminpage.replace('<names/>', nms)
    with open('resources/names.html', 'w') as f:
        f.write(adminpage)
        
    
if __name__=="__main__":
    generate_locations()
    generate_names()