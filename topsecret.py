from flask import Flask, escape, request, redirect
import uuid
import os
import datetime
import json
import random
import send_email
import database
import traceback

from html import escape

app = Flask(__name__)
with open('sm40088_coversecret.png', 'rb') as f:
    secret = f.read()
with open("resources/loginpage.html") as f:
    loginpage = f.read()
with open("resources/names.html") as f:
    adminpage = f.read()
with open("resources/database.html") as f:
    databasepage = f.read()

@app.route('/')
def home():
    return redirect('/static/file-1824.html?user='+str(uuid.uuid4()).replace('-', ''))

@app.route('/wbr-2')
def getnumber():
    time = datetime.datetime.now()
    second = int((((time.hour*60)+time.minute)*60)+time.second)

    if (second < len(secret)):
        number = secret[second]
    else:
        number = 0
    return json.dumps({
        'number': number,
        'index': second
    })

@app.route('/wbr-12', methods=('POST',))
def analyticts():
    #ignore
    return '{}'

@app.route('/wbr-4')
@app.route('/wbr-7')
@app.route('/wbr-10')
def getnumber_bad():
    time = datetime.datetime.now()
    second = int((((time.hour*60)+time.minute)*60)+time.second)
    return json.dumps({
        'number': random.randint(0,128),
        'index': second
    })


@app.route('/qrw-sts-5')
def solution():
    if request.args.get('key', '')=='4w8Rh@9NqCsn':
        resp = redirect('/adminpage')
        resp.set_cookie('secret', str(uuid.uuid4()))
        return resp
    else:
        return loginpage

@app.route('/adminpage')
def adminpageroute():
    if request.cookies.get('secret',''):
        
        return adminpage
    else:
        return 'not authorized'

@app.route('/tq-2')
def special():
    time = datetime.datetime.now()
    hour=time.hour
    words = [
        'SLASH', #0:
        'Stockholm', #1::
        'Tundra', #2:
        'Application', #3:
        'Tundra', #4:
        'Iceberg', #5
        'Congratulations', #6
        'SLASH', #7
        'Application', #8
        'Congratulations', #9
        'Congratulations', #10,
        'Olfactory', #11
    ]
    return words[hour%12]

@app.route('/contact')
def contact():
    if request.args.get('message').lower()!='sales figures are down.':
        return redirect('/static/acco?message=Thank+you')
    else:
        try:
            send_email.send_email(request.args.get('email'),
                                  '/wf-128-b?auth='+str(uuid.uuid4()),
                                  'Welcome on board')
        except:
            print(ex)
            return redirect('/static/acco?message=Invalid+email')
    
    return redirect('/static/acco?message=You+will+back+hear+from+us+shortly')    

@app.route('/wf-128-b')
def dbadmin_redirect():
    return redirect('/wf-128-b/subjects')

@app.route('/wf-128-b/<table>', methods=["GET"])
def dbadmin(table):
    try:
        if (table == 'subjects'):
            filters = [
                'name',
                'age',
                'location',
                'agent'
            ]
            data = database.subject_query(
                name=request.args.get('name', None),
                age=request.args.get('age', None),
                location=request.args.get('location', None),
                agent=request.args.get('agent', None)
            )

            headers = ['name', 'age', 'location', 'agent']
        elif (table == 'organizations'):
            filters = ['name', 'location']
            data = database.organization_query(
                name=request.args.get('name', None),
                location=request.args.get('location', None)
            )
            headers = ['name', 'location']
        elif (table == 'agents'):
            filters = ['name', 'age', 'codename', 'location']
            data = database.agent_query(
                name=request.args.get('name', None),
                age=request.args.get('age', None),
                location=request.args.get('location', None),
                codename=request.args.get('codename', None)
            )
            headers = ['name', 'age', 'codename', 'location']
        elif table == 'pages':
            filters = ('url')
            data = database.page_query(
                url=request.args.get('url', None)
            )
            headers=['url']
        else:
            filters=[]
            headers=["error"]
            data=[["Invalid Table"]]
    except Exception as ex:
        print(ex)
        
        data = [["", ex]]
        headers=["error"]
    formateddata = ''.join(
        '<tr>'+''.join('<td><a href="'+escape(str(i[0]))+'">'+escape(str(r))+'</a></td>'
         for r in i[1:]
        )+'</tr>'
    for i in data)


    filters = ''.join(
        '<label>'+escape(i.capitalize())+':<input name="'+escape(i)+'" value="'+escape(request.args.get(i, ''))+'"></label>'
        for i in filters)

    headers = ''.join(
        '<th>'+escape(i.capitalize())+'</th>'
        for i in headers
    )
    

    html = databasepage
    html = html.replace(
        '<table-contents/>', formateddata
    )
    html = html.replace(
        '<headers/>', headers
    )
    html = html.replace(
        '<filters/>', filters
    )
    return html

@app.route(
    '/sql-secret-page-245'
)
@app.route(
    '/sql-secret-page-245/'
)
def redirect_to_you_win():
    return redirect(
        '/static/you-win-I-guess.html'
    )


@app.route('/secret/highscore',methods=['GET', 'POST'])
def highscore():
    if (request.method == 'GET'):
        with open(os.path.join('resources','highscores.txt')) as f:
            return json.dumps(f.readlines())
    else:
        name = request.form["name"]
        if (name and request.cookies.get('secret', None)):
            with open(os.path.join('resources','highscores.txt'), 'a') as f:
                f.write(name.replace('\n','')[:40]+'\n')
        
        return redirect(
            '/static/you-win-I-guess.html'
        )
