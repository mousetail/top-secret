import sqlite3
import json
import random

def get_name(names):
    return (random.choice(names)+' '+random.choice('abcdefghijklmnopqrstuvwxyz').upper())

def seed():
    db = sqlite3.connect('db.sqlite3')

    with open ('resources/firstnames.json') as f:
        firstnames = json.load(f)
    with open('resources/cities.json', encoding='utf-8') as f:
        cities = [i["name"]+', '+i["country"] for i in json.load(f) if random.randint(0,12)==0]
    with open('resources/words.txt', encoding='utf-8') as f:
        words = [i for i in f.read().split() if i]
    
    print(len(firstnames), len(cities))
    cursor = db.cursor()

    names = [

        ]

    cursor.executescript(
    """
CREATE TABLE IF NOT EXISTS subjects (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name varchar(40) NOT NULL,
    location varchar(40) NOT NULL,
    age INTEGER NOT NULL,
    handler_id INTEGER,
    organization_id INTEGER
);

CREATE TABLE IF NOT EXISTS agents (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name varchar(40) NOT NULL,
    age INTEGER NOT NULL,
    codename varchar(40) NOT NULL,
    location varchar(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS organizations (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name varchar(40),
    location varchar(40)
);
    """
        )

    organization_names = [
        'Organization 1',
        'Organization 2',
        'Organization 2 - International Division',
        'Organization 2 - Leaders',
        'Fire Group',
        'Organization 3',
        'Organization X',
        'Organization X - White Collar',
        'SLFKN'
    ]
    organization_names.sort()

    organizations = []
    for index, i in enumerate(organization_names):
        organizations.append(
            (index, i, random.choice(cities))   
        )

    agents = []
    for i in range(50):
        agents.append(
            (i,
             get_name(firstnames),
             random.randrange(18, random.randrange(20, 60)),
             'The '+random.choice(words).capitalize(),
             random.choice(cities)
             )
        )
    
    subjects = []
    
    for i in range(200):
        agent = random.choice(agents)
        organization = random.choice(organizations)

        location = random.choice((agent[4], organization[2]))

        subjects.append((
            i,
            get_name(firstnames),
            location,
            random.randrange(18, random.randrange(20, random.randrange(40, 100))),
            agent[0],
            organization[0]
            ))

        
    cursor.executemany(
        'INSERT INTO subjects(name, location, age, handler_id, organization_id)'
        ' VALUES (?, ?, ?, ?, ?);',
        [i[1:] for i in subjects]
    )

    cursor.executemany(
        'INSERT INTO agents(name, age, codename, location)'
        ' VALUES (?, ?, ?, ?);',
        [i[1:] for i in agents]
    )

    cursor.executemany(
        'INSERT INTO organizations(name, location) VALUES'
        '(?, ?);',
        [i[1:] for i in organizations]
    )
    cursor.close()
    db.commit()
    db.close()
    

if __name__=="__main__":
    seed()
